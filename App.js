import React from "react";
import { StyleSheet } from "react-native";
import Login from "./src/Login";
import Verify from "./src/Verify";
import HeaderTabs from "./src/MainContent/HeaderTabs";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import KYC from "./src/MainContent/KYC";
import SetupPayment from "./src/MainContent/SetupPayment";
import Register from "./src/Register";
import VerifyLogin from "./src/VerifyLogin";
import Confirm from "./src/MainContent/Confirm";
import Splash from './src/Splash';
const Stack = createStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator
      initialRouteName="Login"
      screenOptions={{ headerShown: null }}
    >
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Verify" component={Verify} />
      <Stack.Screen name="VerifyLogin" component={VerifyLogin} />
      <Stack.Screen name="HeaderTabs" component={HeaderTabs} />
      <Stack.Screen name="KYC" component={KYC} />
      <Stack.Screen name="SetupPayment" component={SetupPayment} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="MyTabs" component={MyTabs} />
      <Stack.Screen name="Confirm" component={Confirm} />
    </Stack.Navigator>
  );
}

import { useFonts } from "@use-expo/font";
import AppLoading from "expo-app-loading";
import MyTabs from "./src/HomeTabs/BottomTabs";

export default function App() {
  let [fontsLoaded] = useFonts({
    "Helvetica-Bold": require("./assets/Fonts/Helvetica-Bold.ttf"),
    "Helvetica-Oblique": require("./assets/Fonts/Helvetica-Oblique.ttf"),
    "Helvetica-BoldOblique": require("./assets/Fonts/Helvetica-BoldOblique.ttf"),
    "Helvetica": require("./assets/Fonts/Helvetica.ttf"),
  });
  if (!fontsLoaded) {
    return <AppLoading />;
  }
  return (
   <NavigationContainer>
      <MyStack />
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});