import React, { useState, useRef } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from "react-native";

const { width, height } = Dimensions.get("window");

export default function Register({ navigation }) {
  return (
    <ScrollView
      style={{ flex: 1, backgroundColor: "white" }}
      showsVerticalScrollIndicator={false}
    >
      <View style={styles.Container}>
        <View style={styles.SecondContainer}>
          <View style={styles.LogoView}>
            <Image
              source={require("../LocallyTImages/LocallyTLogo.png")}
              style={{ height: 95, width: 95, borderRadius: 0 }}
            />
          </View>
          <Text style={styles.Fregister}>Register</Text>
        </View>
        <View style={styles.ThirdContainer}>
          <View
            style={{
              height: 55,
              width: width - 66,
              marginTop: 50,
            }}
          >
            <View
              style={{
                height: 45,
                width: width - 65,
                backgroundColor: "#cfe2f3",
                borderWidth: 1,
                borderColor: "#253f67",
                borderRadius: 20,
                alignItems: "center",
                // justifyContent: "center",
                flexDirection: "row",
                marginTop: 10,
              }}
            >
              <View
                style={{
                  width: "14%",
                  alignItems: "center",
                  justifyContent: "center",
                  height: "100%",
                  borderRightWidth: 1,
                  borderTopStartRadius: 10,
                  borderBottomStartRadius: 10,
                }}
              >
                <Text
                  style={{
                    color: "#253f67",
                    fontFamily: "Helvetica-Bold",
                    fontSize: 18,
                  }}
                >
                  +91
                </Text>
              </View>
              <TextInput
                placeholder="Enter 10 digit mobile number"
                keyboardType="phone-pad"
                maxLength={10}
                style={{
                  height: 41.3,
                  width: "86%",
                  backgroundColor: "#cfe2f3",
                  borderColor: "#253f67",
                  borderRadius: 20,
                  paddingTop: 2.5,
                  color: "#253f67",
                  fontFamily: "Helvetica-Bold",
                  fontSize: 14,
                  paddingLeft: 10,
                }}
              />
            </View>
          </View>
          <TouchableOpacity
            onPress={() => navigation.navigate("Verify")}
            style={styles.RegBut}
          >
            <Text style={styles.RegText}>Register</Text>
          </TouchableOpacity>
          <View style={styles.GoLog}>
            <Text style={styles.GoText}> Already connected to LocallyT?</Text>
            <TouchableOpacity onPress={() => navigation.navigate("Login")}>
              <View style={styles.Log}>
                <Text
                  style={{
                    color: "#253f67",
                    lineHeight: 29,
                    fontSize: 18,
                    fontFamily: "Helvetica-Bold",
                  }}
                >
                  Login
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  Container: {
    height: height,
    width: width,
    alignItems: "center",
    justifyContent: "flex-end",
    backgroundColor: "#253f67",
  },
  SecondContainer: {
    width: width,
    flex: 0.4,
    backgroundColor: "transparent",
    alignItems: "center",
    justifyContent: "center",
  },
  LogoView: {
    width: 130,
    height: 130,
    alignItems: "center",
    marginTop: 25,
    backgroundColor: "white",
    borderRadius: 100,
    justifyContent: "center",
  },
  Fregister: {
    color: "white",
    marginLeft: 10,
    fontSize: 23,
    fontFamily: "Helvetica-Bold",
    textAlign: "center",
    marginTop: 20,
  },
  ThirdContainer: {
    flex: 0.6,
    backgroundColor: "white",
    width: width,
    borderTopStartRadius: 100,
    alignItems: "center",
  },
  PhoneContainer: {
    height: 55,
    width: width - 65,
    backgroundColor: "#cfe2f3",
    borderWidth: 1,
    borderColor: "#253f67",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    marginTop: 75,
  },
  PhoneContainer2: {
    width: "20%",
    alignItems: "center",
    justifyContent: "center",
    height: "100%",
    borderRightWidth: 1,
  },
  PhoneText: {
    height: 50,
    width: "80%",
    backgroundColor: "#cfe2f3",
    borderColor: "#253f67",
    borderRadius: 20,
    paddingTop: 2.5,
    color: "#253f67",
    fontFamily: "Helvetica",
    fontSize: 16,
    paddingLeft: 20,
  },
  RegBut: {
    height: 40,
    width: width - 190,
    backgroundColor: "#fb5414",
    borderRadius: 39,
    justifyContent: "center",
    marginTop: 20,
  },
  RegText: {
    color: "white",
    fontFamily: "Helvetica-Bold",
    textAlign: "center",
    fontSize: 17,
  },
  GoLog: {
    height: 70,
    width: width,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    marginTop: 3,
  },
  GoText: {
    color: "#fb5414",
    right: 12,
    fontFamily: "Helvetica",
    fontSize: 15,
  },
  Log: {
    height: 27,
    width: 53,
    alignItems: "center",
    borderBottomWidth: 2,
    borderColor: "#253f67",
    right: 8,
  },
});
