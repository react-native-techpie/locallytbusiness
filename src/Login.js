import React, {useState} from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from "react-native";
import { Checkbox } from "react-native-paper";
import Onboarding from "./Onboarding/Onboarding";

const { width, height } = Dimensions.get("window");

export default function Login({ navigation }) {
  const [email, setEmail] = useState("");
  const [checked, setChecked] = useState(true);
  return (
    <ScrollView
      style={{ flex: 1, backgroundColor: "white" }}
      showsVerticalScrollIndicator={false}
    >
      <View style={styles.Container}>
        <View style={styles.SecondContainer}>
          <Onboarding />
        </View>
        <View style={styles.MainLogoView}>
          <View
            style={{
              width: width,
              alignItems: "center",
              marginTop: 6,
            }}
          >
            <Image
              source={require("../LocallyTImages/LocallyTLogo.png")}
              style={{ height: 93, width: 93 }}
            />
          </View>
          <View
            style={{
              height: 55,
              width: width - 66,
            }}
          >
            <View
              style={{
                height: 45,
                width: width - 65,
                backgroundColor: "#cfe2f3",
                borderWidth: 1,
                borderColor: "#253f67",
                borderRadius: 20,
                alignItems: "center",
                // justifyContent: "center",
                flexDirection: "row",
                marginTop: 10,
              }}
            >
              <View
                style={{
                  width: "14%",
                  alignItems: "center",
                  justifyContent: "center",
                  height: "100%",
                  borderRightWidth: 1,
                  borderTopStartRadius: 10,
                  borderBottomStartRadius: 10,
                }}
              >
                <Text
                  style={{
                    color: "#253f67",
                    fontFamily: "Helvetica-Bold",
                    fontSize: 18,
                  }}
                >
                  +91
                </Text>
              </View>
              <TextInput
                placeholder="Enter 10 digit mobile number"
                keyboardType="phone-pad"
                maxLength={10}
                onChangeText={setEmail}
                style={{
                  height: 41.3,
                  width: "86%",
                  backgroundColor: "#cfe2f3",
                  borderColor: "#253f67",
                  borderRadius: 20,
                  paddingTop: 2.5,
                  color: "#253f67",
                  fontFamily: "Helvetica-Bold",
                  fontSize: 14,
                  paddingLeft: 10,
                }}
              />
            </View>
          </View>
          <TouchableOpacity
            disabled={email ? false : true}
            disabled={checked ? false : true}
            onPress={() => navigation.navigate("VerifyLogin")}
            style={styles.LoginButton}
          >
            <Text style={styles.LoginText}>Login</Text>
          </TouchableOpacity>
          <View style={styles.NewView}>
            <Text
              style={{
                color: "#fb5414",
                fontFamily: "Helvetica",
                fontSize: 16,
              }}
            >
              {" "}
              New to LocallyT ?{" "}
            </Text>
            <TouchableOpacity onPress={() => navigation.navigate("Register")}>
              <View style={styles.RegisterView}>
                <Text style={styles.Register}>Register</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View
            style={{
              width: width - 55,
              alignItems: "flex-start",
              justifyContent: "space-between",
              flexDirection: "row",
              marginTop: 7,
              backgroundColor: "white",
              height: 70,
            }}
          >
            <Checkbox
              onChangeText={setEmail}
              color="#253f67"
              style={{ color: "red" }}
              status={checked ? "checked" : "unchecked"}
              onPress={() => {
                setChecked(!checked);
              }}
            />
            <View style={{ width: "90%" }}>
              <Text
                style={{
                  color: "#253f67",
                  lineHeight: 20,
                  fontSize: 15,
                  fontFamily: "Helvetica",
                  marginLeft: 5,
                }}
              >
                {" "}
                I have read and agreed to the LocallyT
              </Text>
              <Text
                style={{
                  color: "#253f67",
                  lineHeight: 20,
                  fontSize: 15,
                  fontFamily: "Helvetica",
                  marginLeft: 5,
                }}
              >
                {" "}
                Terms and Conditions, Privacy Policies
              </Text>
              <Text
                style={{
                  color: "#253f67",
                  lineHeight: 20,
                  fontSize: 15,
                  fontFamily: "Helvetica",
                  marginLeft: 5,
                }}
              >
                {" "}
                and User Agreement.
              </Text>
            </View>
          </View>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  Container: {
    height: height,
    width: width,
    alignItems: "center",
    justifyContent: "flex-end",
    backgroundColor: "#253f67",
    paddingBottom: 0,
  },

  SecondContainer: {
    width: width,
    flex: 0.4,
    backgroundColor: "transparent",
    alignItems: "center",
    justifyContent: "center",
  },
  MainLogoView: {
    flex: 0.6,
    backgroundColor: "white",
    width: width,
    borderTopStartRadius: 100,
    alignItems: "center",
  },
  SecondPhone: {
    width: "20%",
    alignItems: "center",
    justifyContent: "center",
    height: "100%",
    borderRightWidth: 1,
  },
  PhoneContainer: {
    height: 50,
    width: width - 65,
    backgroundColor: "#cfe2f3",
    borderWidth: 1,
    borderColor: "#253f67",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    marginTop: 8,
  },
  PhoneText: {
    height: 48,
    width: "80%",
    backgroundColor: "#cfe2f3",
    borderColor: "#253f67",
    borderRadius: 20,
    paddingTop: 2.5,
    color: "#253f67",
    fontFamily: "Helvetica-Bold",
    fontSize: 17,
    paddingLeft: 20,
  },
  LoginButton: {
    height: 40,
    width: width - 190,
    backgroundColor: "#fb5414",
    borderRadius: 39,
    justifyContent: "center",
    marginTop: 20,
  },
  LoginText: {
    color: "white",
    fontFamily: "Helvetica-Bold",
    textAlign: "center",
    fontSize: 17,
  },
  NewView: {
    height: 70,
    width: width,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
  },
  RegisterView: {
    height: 27,
    width: 75,
    alignItems: "center",
    borderBottomWidth: 2,
    borderColor: "#253f67",
  },
  Register: {
    color: "#253f67",
    lineHeight: 27,
    fontSize: 18,
    fontFamily: "Helvetica-Bold",
  },
  ConditionView: {
    width: width - 65,
    alignItems: "flex-start",
    justifyContent: "space-between",
    flexDirection: "row",
    marginTop: 18,
    backgroundColor: "white",
  },
  ConditionText: {
    color: "#253f67",
    lineHeight: 20,
    fontSize: 16.5,
    fontFamily: "Helvetica",
    marginLeft: 10,
  },
});
