import React, {useState} from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  Dimensions,
  TextInput,
  TouchableOpacity,
  Keyboard,
  ScrollView,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import { Picker } from "@react-native-picker/picker";


const { height, width } = Dimensions.get("window");

export default function BusinessDetails({navigation}) {
 const [selectedValue, setSelectedValue] = useState("Grocery store");

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View
        style={{
          width: width,
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "white",
          flex: 1,
        }}
      >
        <ScrollView showsVerticalScrollIndicator={false} style={styles.Scroll}>
          <View style={styles.Container}>
            <View
              style={{
                // height: "100%",
                backgroundColor: "white",
                width: width,
                borderTopStartRadius: 20,
                borderTopEndRadius: 20,
                paddingTop: 15,
                // paddingLeft: 10,
                alignItems: "center",
                paddingRight: 3,
              }}
            >
              <View style={{ width: "90%", height: 100 }}>
                <Text style={{ fontSize: 16, fontFamily: "Helvetica-Bold" }}>
                  Enter name of your business
                </Text>
                <TextInput
                  style={{
                    height: 49,
                    width: width - 30,
                    borderRadius: 16,
                    backgroundColor: "white",
                    elevation: 1,
                    marginTop: 13,
                    borderWidth: 1.5,
                    borderColor: "#cfe2f3",
                    paddingLeft: 18,
                    fontFamily: "Helvetica",
                    fontSize: 16,
                  }}
                  placeholder="Name of your business"
                />
              </View>
              <View style={{ width: "90%", height: 100 }}>
                <Text style={{ fontSize: 16, fontFamily: "Helvetica-Bold" }}>
                  Select business category
                </Text>
                <View
                  style={{
                    height: 49,
                    width: width - 30,
                    borderRadius: 16,
                    backgroundColor: "white",
                    elevation: 1,
                    marginTop: 16,
                    borderWidth: 1.5,
                    borderColor: "#cfe2f3",
                    paddingLeft: 10,
                    fontFamily: "Helvetica-Bold",
                    paddingBottom: 2,
                    justifyContent: "center",
                  }}
                >
                  <Picker
                    style={{ height: 36 }}
                    selectedValue={selectedValue}
                    onValueChange={(itemValue, itemIndex) =>
                      setSelectedValue(itemValue)
                    }
                  >
                    <Picker.Item label="Grocery Stores" value="Grocery store" />
                    <Picker.Item label="Medicines" value="Hospital" />
                    <Picker.Item label="Restaurants" value="Police Station" />
                    <Picker.Item label="General Stores" value="School" />
                  </Picker>
                </View>
              </View>
              <View style={{ width: "90%", height: 100, marginTop: 7 }}>
                <Text style={{ fontSize: 16, fontFamily: "Helvetica-Bold" }}>
                  Enter working hours
                </Text>
                <View
                  style={{
                    width: "100%",
                    alignItems: "center",
                    justifyContent: "space-between",
                    flexDirection: "row",
                  }}
                >
                  <View
                    style={{
                      height: 50,
                      width: width - 220,
                      borderRadius: 16,
                      backgroundColor: "white",
                      marginTop: 16,
                      borderWidth: 1.5,
                      borderColor: "#cfe2f3",
                      paddingLeft: 0,
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <TextInput
                      keyboardType="default"
                      style={{
                        height: 34,
                        width: "56%",
                        borderRadius: 20,
                        fontFamily: "Helvetica",
                      }}
                      placeholder="Enter Time"
                    />
                    <View
                      style={{
                        height: 43,
                        width: "34%",
                        borderRadius: 0,
                        backgroundColor: "white",
                        borderLeftWidth: 2,

                        borderColor: "#cfe2f3",
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <Text
                        style={{ fontSize: 16, fontFamily: "Helvetica-Bold" }}
                      >
                        AM
                      </Text>
                    </View>
                  </View>
                  <Text
                    style={{
                      fontSize: 16,
                      marginTop: 11,
                      fontFamily: "Helvetica-Bold",
                      // marginLeft: 20,
                    }}
                  >
                    To
                  </Text>
                  <View
                    style={{
                      height: 50,
                      width: width - 220,
                      borderRadius: 16,
                      backgroundColor: "white",
                      marginTop: 16,
                      borderWidth: 1.5,
                      borderColor: "#cfe2f3",
                      paddingLeft: 0,
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <TextInput
                      keyboardType="default"
                      style={{
                        height: 34,
                        width: "56%",
                        borderRadius: 20,
                        fontFamily: "Helvetica",
                      }}
                      placeholder="Enter Time"
                    />
                    <View
                      style={{
                        height: 43,
                        width: "34%",
                        borderRadius: 0,
                        backgroundColor: "white",
                        borderLeftWidth: 2,

                        borderColor: "#cfe2f3",
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <Text
                        style={{ fontSize: 16, fontFamily: "Helvetica-Bold" }}
                      >
                        PM
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
              <View style={{ width: "90%", height: 100, marginTop: 7 }}>
                <Text style={{ fontSize: 16, fontFamily: "Helvetica-Bold" }}>
                  Enter location on map
                </Text>
                <View
                  style={{
                    height: 50,
                    width: width - 30,
                    borderRadius: 15,
                    backgroundColor: "white",
                    // elevation: 0.9,
                    marginTop: 11,
                    borderWidth: 1.5,
                    borderColor: "#cfe2f3",
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <TextInput
                    style={{
                      height: 40,
                      width: width - 90,
                      borderRadius: 10,
                      paddingLeft: 15,
                      fontFamily: "Helvetica",
                    }}
                    placeholder="Search for Area / LocallyT"
                  />
                  <View
                    style={{
                      height: 36,
                      width: 45,
                      borderLeftWidth: 1,
                      borderColor: "gray",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <Icon name="search" color="gray" size={20} />
                  </View>
                </View>
              </View>

              <View
                style={{
                  width: "90%",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                  height: 60,
                  // backgroundColor: "red",
                  marginTop: 0,
                }}
              >
                <View style={{ width: "48%" }}>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: "Helvetica-Bold",
                      lineHeight: 19,
                    }}
                  >
                    Select time need for your business to home deliver items
                    after receiving order
                  </Text>
                </View>
                <View
                  style={{
                    height: 50,
                    width: "54%",
                    borderRadius: 16,
                    backgroundColor: "white",
                    elevation: 0,
                    marginTop: 1,
                    borderWidth: 1.5,
                    borderColor: "#cfe2f3",
                    paddingLeft: 10,
                    fontFamily: "Helvetica-Bold",
                    paddingBottom: 2,
                    // alignItems:'center',
                    justifyContent: "center",
                  }}
                >
                  <Picker
                    style={{ height: 36, fontSize: 11 }}
                    selectedValue={selectedValue}
                    onValueChange={(itemValue, itemIndex) =>
                      setSelectedValue(itemValue)
                    }
                  >
                    <Picker.Item label="Within 1 hrs" value="Within 1 hrs" />
                    <Picker.Item label="Within 2 hrs" value="Within 2 hrsl" />
                    <Picker.Item label="Within 3 hrs" value="Within 3 hrsl" />
                    <Picker.Item label="Within 5 hrs" value="Within 5 hrs" />
                  </Picker>
                </View>
              </View>

              {/* <View
              style={{
                width: "95%",
                height: 50,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "flex-start",
              }}
            >
              <Icon
                name="compass"
                color="#fb5414"
                size={25}
                style={{ marginLeft: 9 }}
              />
              <Text
                style={{
                  fontSize: 19,
                  marginLeft: 20,
                }}
              >
                Use my current location
              </Text>
            </View>
            <View
              style={{
                height: 200,
                width: width - 20,
                borderRadius: 15,
                backgroundColor: "white",
                marginTop: 16,
                borderWidth: 1.3,
                borderColor: "#cfe2f3",
                flexDirection: "row",
                elevation: 2,
              }}
            ></View> */}

              {/* <View style={{ width: width, height: 30 }}></View> */}
            </View>
          </View>
        </ScrollView>
        <View
          style={{
            width: width,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <TouchableOpacity
            onPress={() => navigation.navigate("KYC")}
            style={{
              height: 40,
              width: width - 190,
              backgroundColor: "#fb5414",
              borderRadius: 39,
              justifyContent: "center",
              marginTop: 0,
            }}
          >
            <Text
              style={{
                color: "white",
                fontFamily: "Helvetica-Bold",
                textAlign: "center",
                fontSize: 17,
              }}
            >
              Continue
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{ width: width, height: 30 }}></View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  Container: {
    // height: "100%",
    paddingTop: 7,
    width: width,
    alignItems: "center",
    // justifyContent: "flex-end",
    backgroundColor: "#253f67",
  },
  Scroll: {
    backgroundColor:'#FFFF'
  }
});

