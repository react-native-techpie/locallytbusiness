import React, { useState, useRef } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  Dimensions,
  TextInput,
  TouchableOpacity,
  Keyboard,
  ScrollView,
  Modal
} from "react-native";

const { height, width } = Dimensions.get("window");

export default function SetupPayment({ navigation }) {
const [modalOpen, setModalOpen] = useState(false);


  const [selectedValue, setSelectedValue] = useState("Grocery store");

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View
        style={{
          width: width,
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "white",
          flex: 1,
        }}
      >
        <Modal
          // visible={true}
          visible={modalOpen}
          transparent={true}
          animationType="slide"
        >
          <TouchableWithoutFeedback onPress={() => setModalOpen(false)}>
            <View
              style={{
                flex: 1,
                backgroundColor: "#000000aa",
                width: width,
                alignItems: "center",
                justifyContent: "flex-end",
              }}
            >
              <View
                style={{
                  height: 170,
                  width: width,
                  backgroundColor: "white",
                  alignItems: "center",
                  justifyContent: "space-around",
                }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    color: "#283673",
                    fontFamily: "Helvetica-Bold",
                    fontSize: 19,
                  }}
                >
                  Select Photo To Upload
                </Text>
                <View
                  style={{
                    flexDirection: "row",
                    width: "80%",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <View
                    style={{
                      width: "50%",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <TouchableOpacity>
                      <Image
                        source={require("../../LocallyTImages/Gallery.png")}
                        style={{ height: 80, width: 80 }}
                      />
                    </TouchableOpacity>
                    <Text
                      style={{
                        textAlign: "center",
                        color: "#283673",
                        marginTop: 0,
                        letterSpacing: 0,
                        fontFamily: "Helvetica-Bold",
                        fontSize: 13,
                      }}
                    >
                      Choose From Gallery
                    </Text>
                  </View>
                  <View
                    style={{
                      width: "50%",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <TouchableOpacity>
                      <Image
                        source={require("../../LocallyTImages/Camera.png")}
                        style={{ height: 80, width: 80 }}
                      />
                    </TouchableOpacity>
                    <Text
                      style={{
                        textAlign: "center",
                        color: "#283673",
                        marginTop: 0,
                        letterSpacing: 0,
                        fontFamily: "Helvetica-Bold",
                        fontSize: 13,
                      }}
                    >
                      Take a Photo
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>

        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{
            height: "100%",
            paddingTop: 7,
            width: width,
            backgroundColor: "#253f67",
          }}
        >
          <View>
            <View
              style={{
                height: "100%",
                backgroundColor: "white",
                width: width,
                borderTopStartRadius: 20,
                borderTopEndRadius: 20,
                paddingTop: 15,
                paddingLeft: 0,
                alignItems: "center",
              }}
            >
              <View
                style={{
                  width: "97%",
                  height: "100%",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <View style={{ width: "95%" }}>
                  <Text style={{ fontSize: 16, fontFamily: "Helvetica-Bold" }}>
                    Select payment modes accepted by your business
                  </Text>

                  <View
                    style={{
                      width: "100%",
                      height: 54,
                      alignItems: "center",
                      justifyContent: "space-evenly",
                      flexDirection: "row",
                    }}
                  >
                    <TouchableOpacity
                      style={{
                        height: 49,
                        width: width - 200,
                        borderRadius: 16,
                        backgroundColor: "white",
                        marginTop: 16,
                        borderWidth: 1.5,
                        borderColor: "#cfe2f3",
                        justifyContent: "center",
                        paddingLeft: 10,
                        alignItems: "center",
                        elevation: 1,
                      }}
                    >
                      <Image
                        source={require("../../LocallyTImages/PayTm.jpg")}
                        style={{ height: 30, width: 100 }}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{
                        height: 49,
                        width: width - 200,
                        borderRadius: 16,
                        backgroundColor: "white",
                        marginTop: 16,
                        borderWidth: 1.5,
                        borderColor: "#cfe2f3",
                        justifyContent: "center",
                        paddingLeft: 10,
                        alignItems: "center",
                        elevation: 1,
                      }}
                    >
                      <Image
                        source={require("../../LocallyTImages/PhonePay.jpg")}
                        style={{ height: 40, width: 120, marginLeft: 0 }}
                      />
                    </TouchableOpacity>
                  </View>

                  <View
                    style={{
                      width: "100%",
                      height: 54,
                      alignItems: "center",
                      justifyContent: "space-evenly",
                      flexDirection: "row",
                      marginTop: 10,
                    }}
                  >
                    <TouchableOpacity
                      style={{
                        height: 49,
                        width: width - 200,
                        borderRadius: 16,
                        backgroundColor: "white",
                        marginTop: 16,
                        borderWidth: 1.5,
                        borderColor: "#cfe2f3",
                        justifyContent: "center",
                        paddingLeft: 10,
                        alignItems: "center",
                        elevation: 1,
                      }}
                    >
                      <Image
                        source={require("../../LocallyTImages/GooglePay.jpg")}
                        style={{ height: 30, width: 88 }}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{
                        height: 49,
                        width: width - 200,
                        borderRadius: 16,
                        backgroundColor: "white",
                        marginTop: 16,
                        borderWidth: 1.5,
                        borderColor: "#cfe2f3",
                        justifyContent: "center",
                        paddingLeft: 10,
                        alignItems: "center",
                        elevation: 1,
                      }}
                    >
                      <Image
                        source={require("../../LocallyTImages/UPI.jpg")}
                        style={{ height: 30, width: 100 }}
                      />
                    </TouchableOpacity>
                  </View>

                  <View
                    style={{
                      width: width - 30,
                      height: 54,
                      alignItems: "center",
                      justifyContent: "space-between",
                      flexDirection: "row",
                      marginTop: 15,
                    }}
                  >
                    <TextInput
                      placeholder="Others"
                      style={{
                        height: 49,
                        width: "100%",
                        borderRadius: 16,
                        backgroundColor: "white",
                        marginTop: 16,
                        borderWidth: 1.5,
                        borderColor: "#cfe2f3",
                        paddingLeft: 20,
                        fontFamily: "Helvetica",
                        elevation: 1,
                      }}
                    />
                  </View>
                  <View style={{ width: "95%", paddingLeft: 8, marginTop: 16 }}>
                    <Text
                      style={{ fontSize: 16, fontFamily: "Helvetica-Bold" }}
                    >
                      Enter mobile number on which you want to receive payments
                    </Text>
                  </View>
                  <View
                    style={{
                      height: 55,
                      width: width - 30,
                    }}
                  >
                    <View
                      style={{
                        height: 45,
                        width: width - 30,
                        borderWidth: 1,
                        borderColor: "#253f67",
                        borderRadius: 20,
                        alignItems: "center",
                        // justifyContent: "center",
                        flexDirection: "row",
                        marginTop: 10,
                        elevation: 1,
                        backgroundColor: "white",
                      }}
                    >
                      <View
                        style={{
                          width: "14%",
                          alignItems: "center",
                          justifyContent: "center",
                          height: "100%",
                          borderRightWidth: 1,
                          borderTopStartRadius: 10,
                          borderBottomStartRadius: 10,
                        }}
                      >
                        <Text
                          style={{
                            color: "#253f67",
                            fontFamily: "Helvetica-Bold",
                            fontSize: 18,
                          }}
                        >
                          +91
                        </Text>
                      </View>
                      <TextInput
                        placeholder="Enter 10 digit mobile number"
                        keyboardType="phone-pad"
                        maxLength={10}
                        style={{
                          height: 41.3,
                          width: "86%",
                          borderColor: "#253f67",
                          borderRadius: 20,
                          paddingTop: 2.5,
                          color: "#253f67",
                          fontFamily: "Helvetica-Bold",
                          fontSize: 14,
                          paddingLeft: 10,
                        }}
                      />
                    </View>
                  </View>
                  <View style={{ width: "95%", paddingLeft: 8 }}>
                    <Text
                      style={{
                        fontSize: 16,
                        fontFamily: "Helvetica-Bold",
                        marginTop: 14,
                      }}
                    >
                      UPI ID to receive payments
                    </Text>
                  </View>
                  <View
                    style={{
                      height: 49,
                      width: width - 30,
                      borderRadius: 16,
                      backgroundColor: "white",
                      elevation: 1,
                      marginTop: 14,
                      borderWidth: 1.5,
                      borderColor: "#cfe2f3",
                      justifyContent: "center",
                      paddingLeft: 10,
                    }}
                  >
                    <TextInput
                      placeholder="UPI ID of your store"
                      style={{
                        height: 45,
                        width: "100%",
                        paddingLeft: 20,
                        fontSize: 15,
                        fontFamily: "Helvetica",
                      }}
                    />
                  </View>
                  <View style={{ width: "90%", paddingLeft: 8 }}>
                    <Text
                      style={{
                        fontSize: 16,
                        fontFamily: "Helvetica-Bold",
                        marginTop: 17,
                      }}
                    >
                      Upload picture of payment QR codes
                    </Text>
                  </View>
                  <View
                    style={{
                      marginTop: 16,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <TouchableOpacity
                      TouchableOpacity
                      onPress={() => setModalOpen(true)}
                      style={{
                        height: 100,
                        width: 100,
                        borderRadius: 22,
                        backgroundColor: "#cfe2f3",
                      }}
                    >
                      <View
                        style={{
                          height: "100%",
                          width: "100%",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <Image
                          source={require("../../LocallyTImages/Uploadcamera.png")}
                          style={{ height: 60, width: 60 }}
                        />
                        <Text
                          style={{
                            textAlign: "center",
                            fontFamily: "Helvetica-Bold",
                          }}
                        >
                          Add Photos
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                  <View style={{ width: width, height: 24 }}></View>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>

        <View
          style={{
            width: width,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <TouchableOpacity
            onPress={() => navigation.navigate("Confirm")}
            style={{
              height: 40,
              width: width - 190,
              backgroundColor: "#fb5414",
              borderRadius: 39,
              justifyContent: "center",
              marginTop: 0,
            }}
          >
            <Text
              style={{
                color: "white",
                fontFamily: "Helvetica-Bold",
                textAlign: "center",
                fontSize: 17,
              }}
            >
              Confirm
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{ width: width, height: 30 }}></View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({});
