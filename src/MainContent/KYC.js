import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  Dimensions,
  TextInput,
  TouchableOpacity,
  Keyboard,
  ScrollView,
  Modal
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import { Picker } from "@react-native-picker/picker";

const { height, width } = Dimensions.get("window");

export default function KYC({ navigation }) {

const [modalOpen, setModalOpen] = useState(false);

  const [selectedValue, setSelectedValue] = useState("Aadhar Card");

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View
        style={{
          width: width,
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "white",
          flex: 1,
        }}
      >
        <Modal
          // visible={true}
          visible={modalOpen}
          transparent={true}
          animationType="slide"
        >
          <TouchableWithoutFeedback onPress={() => setModalOpen(false)}>
            <View
              style={{
                flex: 1,
                backgroundColor: "#000000aa",
                width: width,
                alignItems: "center",
                justifyContent: "flex-end",
              }}
            >
              <View
                style={{
                  height: 170,
                  width: width,
                  backgroundColor: "white",
                  alignItems: "center",
                  justifyContent: "space-around",
                }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    color: "#283673",
                    fontFamily: "Helvetica-Bold",
                    fontSize: 19,
                  }}
                >
                  Select Photo To Upload
                </Text>
                <View
                  style={{
                    flexDirection: "row",
                    width: "80%",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <View
                    style={{
                      width: "50%",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <TouchableOpacity>
                      <Image
                        source={require("../../LocallyTImages/Gallery.png")}
                        style={{ height: 80, width: 80 }}
                      />
                    </TouchableOpacity>
                    <Text
                      style={{
                        textAlign: "center",
                        color: "#283673",
                        marginTop: 0,
                        letterSpacing: 0,
                        fontFamily: "Helvetica-Bold",
                        fontSize: 13,
                      }}
                    >
                      Choose From Gallery
                    </Text>
                  </View>
                  <View
                    style={{
                      width: "50%",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <TouchableOpacity>
                      <Image
                        source={require("../../LocallyTImages/Camera.png")}
                        style={{ height: 80, width: 80 }}
                      />
                    </TouchableOpacity>
                    <Text
                      style={{
                        textAlign: "center",
                        color: "#283673",
                        marginTop: 0,
                        letterSpacing: 0,
                        fontFamily: "Helvetica-Bold",
                        fontSize: 13,
                      }}
                    >
                      Take a Photo
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>

        <ScrollView showsVerticalScrollIndicator={false} style={styles.Scroll}>
          <View style={styles.Container}>
            <View
              style={{
                height: height,
                backgroundColor: "white",
                width: width,
                borderTopStartRadius: 20,
                borderTopEndRadius: 20,
                paddingTop: 15,
                alignItems: "center",
              }}
            >
              <View style={{ width: "90%", height: 100 }}>
                <Text style={{ fontSize: 16, fontFamily: "Helvetica-Bold" }}>
                  Upload Personal KYC Document
                </Text>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-around",
                    height: 70,
                  }}
                >
                  <View
                    style={{
                      height: 50,
                      width: width - 120,
                      borderRadius: 16,
                      backgroundColor: "white",
                      elevation: 1,
                      marginTop: 16,
                      borderWidth: 1.5,
                      borderColor: "#cfe2f3",
                      paddingLeft: 10,
                      fontFamily: "Helvetica-Bold",
                    }}
                    placeholder="Name of your business"
                  >
                    <Picker
                      style={{ height: 45, fontFamily: "Helvetica-Bold" }}
                      selectedValue={selectedValue}
                      onValueChange={(itemValue, itemIndex) =>
                        setSelectedValue(itemValue)
                      }
                    >
                      <Picker.Item
                        style={{ fontFamily: "Helvetica-Bold" }}
                        label="Aadhar Card"
                        value="Aadhar Card"
                      />
                      <Picker.Item label="Pan Card" value="Pan Card" />
                      <Picker.Item label="Voting Card" value="Voting Card" />
                      <Picker.Item label="Passport" value="Passport" />
                    </Picker>
                  </View>
                  <View
                    style={{
                      height: 60,
                      width: 60,
                      borderWidth: 0.9,
                      borderColor: "#fb5414",
                      borderRadius: 16,
                      backgroundColor: "white",
                      elevation: 1,
                      // marginLeft: 40,
                      alignItems: "center",
                      justifyContent: "center",
                      marginTop: 11,
                    }}
                  >
                    <TouchableOpacity onPress={() => setModalOpen(true)}>
                      <Image
                        source={require("../../LocallyTImages/Uploadcamera.png")}
                        style={{ height: 50, width: 50 }}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              <View style={{ width: "90%", height: 100, marginTop: 15 }}>
                <Text style={{ fontSize: 16, fontFamily: "Helvetica-Bold" }}>
                  Upload Business KYC Document
                </Text>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-around",
                    height: 70,
                  }}
                >
                  <View
                    style={{
                      height: 50,
                      width: width - 120,
                      borderRadius: 16,
                      backgroundColor: "white",
                      elevation: 1,
                      marginTop: 16,
                      borderWidth: 1.5,
                      borderColor: "#cfe2f3",
                      paddingLeft: 10,
                      fontFamily: "Helvetica-Bold",
                    }}
                    placeholder="Name of your business"
                  >
                    <Picker
                      style={{ height: 45, fontFamily: "Helvetica-Bold" }}
                      selectedValue={selectedValue}
                      onValueChange={(itemValue, itemIndex) =>
                        setSelectedValue(itemValue)
                      }
                    >
                      <Picker.Item
                        style={{ fontFamily: "Helvetica-Bold" }}
                        label="Aadhar Card"
                        value="Aadhar Card"
                      />
                      <Picker.Item label="Pan Card" value="Pan Card" />
                      <Picker.Item label="Voting Card" value="Voting Card" />
                      <Picker.Item label="Passport" value="Passport" />
                    </Picker>
                  </View>
                  <View
                    style={{
                      height: 60,
                      width: 60,
                      borderWidth: 0.9,
                      borderColor: "#fb5414",
                      borderRadius: 16,
                      backgroundColor: "white",
                      elevation: 1,
                      // marginLeft: 40,
                      alignItems: "center",
                      justifyContent: "center",
                      marginTop: 11,
                    }}
                  >
                    <TouchableOpacity
                      TouchableOpacity
                      onPress={() => setModalOpen(true)}
                    >
                      <Image
                        source={require("../../LocallyTImages/Uploadcamera.png")}
                        style={{ height: 50, width: 50 }}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>

              <View
                style={{
                  width: "90%",
                  height: 50,
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "flex-start",
                  marginTop: 5,
                }}
              >
                <Icon
                  name="compass"
                  color="#fb5414"
                  size={25}
                  style={{ marginLeft: 9 }}
                />
                <Text
                  style={{
                    fontSize: 16,
                    fontFamily: "Helvetica-Bold",
                    left: 20,
                  }}
                >
                  Use my current location
                </Text>
              </View>
              <View
                style={{
                  width: width,
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <View
                  style={{
                    height: 180,
                    width: "90%",
                    borderRadius: 15,
                    backgroundColor: "white",
                    marginTop: 6,
                    borderWidth: 1.5,
                    borderColor: "#cfe2f3",
                    flexDirection: "row",
                    elevation: 1,
                  }}
                ></View>
              </View>
            </View>
          </View>
        </ScrollView>
        <View
          style={{
            width: width,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <TouchableOpacity
            onPress={() => navigation.navigate("SetupPayment")}
            style={{
              height: 40,
              width: width - 190,
              backgroundColor: "#fb5414",
              borderRadius: 39,
              justifyContent: "center",
              marginTop: 0,
            }}
          >
            <Text
              style={{
                color: "white",
                fontFamily: "Helvetica-Bold",
                textAlign: "center",
                fontSize: 17,
              }}
            >
              Continue
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{ width: width, height: 30 }}></View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  Container: {
    height: "100%",
    paddingTop: 7,
    width: width,
    alignItems: "center",
    // justifyContent: "flex-end",
    backgroundColor: "#253f67",
    // paddingBottom: 16,
  },
  Scroll: {
    backgroundColor: "#FFFF",
  },
});
