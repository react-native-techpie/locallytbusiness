import React from "react";
import { View, Text, StyleSheet, Dimensions, Image } from "react-native";

const { height, width } = Dimensions.get("window");


import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { NavigationContainer } from "@react-navigation/native";
import Today from "./Dashboard/Today";
import Historical from "./Dashboard/Historical";
import Login from "../Login";
import Register from "../Register";
import Profile from "./Profile";
import NewRequst from './OrderFlow/NewRequest';
import DashBoardHeader from "./Dashboard/DashBoardHeader";
import OrderHeader from "./OrderFlow/OrderHeader";

const Tab = createBottomTabNavigator();

function MyTabs() {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false,
        tabBarStyle: {
          backgroundColor: "#FFFF",
          height: 58,
          elevation: 20,
          bottom: 0,
        },
      }}
    >
      <Tab.Screen
        name="DashBoardHeader"
        component={DashBoardHeader}
        options={{
          tabBarIcon: ({ focused }) => (
            <View
              style={{ justifyContent: "center", alignItem: "center", left: 4 }}
            >
              <Image
                source={require("../../LocallyTImages/Dashboard.png")}
                resizeMode="contain"
                style={{
                  width: 30,
                  height: 30,
                  alignContent: "center",
                  tintColor: focused ? "#fb5414" : "black",
                  alignSelf: "center",
                }}
              />
              <Text
                style={{
                  fontSize: 12,
                  fontFamily: "Helvetica-Bold",
                  color: focused ? "#fb5414" : "black",
                }}
              >
                Today's Report
              </Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="OrderHeader"
        component={OrderHeader}
        options={{
          tabBarIcon: ({ focused }) => (
            <View style={{ justifyContent: "center", alignItem: "center" }}>
              <Image
                source={require("../../LocallyTImages/order.png")}
                resizeMode="contain"
                style={{
                  width: 30,
                  height: 30,
                  alignContent: "center",
                  tintColor: focused ? "#fb5414" : "black",
                  alignSelf: "center",
                }}
              />
              <Text
                style={{
                  fontSize: 12,
                  fontFamily: "Helvetica-Bold",
                  color: focused ? "#fb5414" : "black",
                }}
              >
                Order Flow
              </Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarIcon: ({ focused }) => (
            <View style={{ justifyContent: "center", alignItem: "center" }}>
              <Image
                source={require("../../LocallyTImages/profile.png")}
                resizeMode="contain"
                style={{
                  width: 30,
                  height: 30,
                  alignContent: "center",
                  tintColor: focused ? "#fb5414" : "black",
                  alignSelf: "center",
                }}
              />
              <Text
                style={{
                  fontSize: 12,
                  fontFamily: "Helvetica-Bold",
                  color: focused ? "#fb5414" : "black",
                }}
              >
                Profile
              </Text>
            </View>
          ),
        }}
      />
    </Tab.Navigator>
  );
}


export default MyTabs;