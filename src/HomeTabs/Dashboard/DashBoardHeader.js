import React from "react";
import { View, Text, StyleSheet, Dimensions, StatusBar } from "react-native";

import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";

import Today from "./Today";
import Historical from "./Historical";

const Tab = createMaterialTopTabNavigator();

export default function DashBoardHeader() {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarStyle: {
          backgroundColor: "#253f67",
          height: 66,
          bottom: 0,
          paddingTop: 7,
        },
      }}
      style={{ paddingTop: StatusBar.currentHeight }}
    >
      <Tab.Screen
        options={{
          title: ({ focused }) => (
            <View
              style={{
                height: 35,
                borderRadius: 30,
                width: 170,
                // marginTop: 19,
                backgroundColor: focused ? "#fb5414" : "#253f67",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  fontFamily: "Helvetica-Bold",
                  fontSize: 16,
                }}
              >
                Today's Report
              </Text>
            </View>
          ),
        }}
        name="BusinessDetails"
        component={Today}
      />
      <Tab.Screen
        options={{
          title: ({ focused }) => (
            <View
              style={{
                height: 35,
                borderRadius: 30,
                width: 170,
                backgroundColor: focused ? "#fb5414" : "#253f67",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  fontFamily: "Helvetica-Bold",
                  fontSize: 16,
                }}
              >
                Historical Reports
              </Text>
            </View>
          ),
        }}
        name="KYC"
        component={Historical}
      />
    </Tab.Navigator>
  );
}
