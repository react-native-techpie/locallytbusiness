import React from "react";
import { View, Text, StyleSheet, Image, Dimensions } from "react-native";

const { height, width } = Dimensions.get("window");

export default function Splash() {
  return (
    <View style={styles.Container}>
        <View style={{justifyContent:'flex-end', height:'50%'}}>
      <Image
        source={require("../LocallyTImages/LocallyTLogo.png")}
        style={{ height: 270, width: 270 }}
      />
      </View>
      <View style={{justifyContent:'flex-end', height:'50%'}}>
        <Image
          source={require("../LocallyTImages/SplashDown.jpg")}
          style={{ height: 257, width: width }}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    height: height,
    width: width,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#FFFF",
  },
});
